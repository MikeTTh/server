#!/bin/sh

DIR=$(pwd)
echo "Building server"
cd $DIR/src/main
go build -o $DIR/server server.go
echo "Building JS"
cd $DIR/src/client-js
$(go env GOPATH)/bin/gopherjs build -m -o $DIR/out/clock.js clockjs.go
echo "Building WASM"
GOOS=js GOARCH=wasm go build -o $DIR/out/clock.wasm clockjs.go