"use strict";

let words = [];
let teams = 0;
let team = [];
const wordel = document.getElementById("word");
const donts = document.getElementById("donts");

function getRandomColor() {
	const letters = '0123456789ABCDEF';
	let color = '#';
	for (let i = 0; i < 6; i++) {
		color += letters[Math.floor(Math.random() * 16)];
	}
	return color;
}

function nextWord() {
	if (words.length <= 0){
		document.getElementById("gamestuff").style.display="none";
		document.getElementById("done").style.display="block";
	}
	const id = Math.floor(Math.random()*words.length);
	const word = words[id];
	words.splice(id, 1);
	wordel.innerText = word.Word;
	while (donts.firstChild){
		donts.removeChild(donts.firstChild);
	}
	for (let i = 0; i < word.DontSay.length; i++) {
		let li = document.createElement("p");
		li.innerText = word.DontSay[i];
		donts.appendChild(li)
	}
}

function drawTeams() {
	let point = document.getElementById("points");
	while (point.firstChild){
		point.removeChild(point.firstChild);
	}
	for (let i = 0; i < team.length; i++){
		let teambut = document.createElement("button");
		teambut.style.background = team[i].color;
		teambut.innerText = "Team "+i+": "+team[i].score+" points";
		teambut.onclick = function () {
			team[i].score = team[i].score + 1;
			drawTeams();
		};
		point.appendChild(teambut);
	}
}

function load(){
	let req = new XMLHttpRequest();
	let url = window.location.protocol + "//" + window.location.host + "/res/apps/tabu/questions.json";
	req.onreadystatechange = function () {
		if (req.readyState === XMLHttpRequest.DONE) {
			words = JSON.parse(req.responseText);
			document.getElementById("load").style.display="none";
			document.getElementById("teamnum").style.display="block";
			document.getElementById("tb").onclick = function () {
				teams = Math.floor(document.getElementById("tn").value);
				if (!(teams>0)) {
					teams = 1
				}
				document.getElementById("teamnum").style.display="none";
				document.getElementById("game").style.display="block";
				for (let i = 1; i<=teams; i++) {
					team.push({
						id: i,
						color: getRandomColor(),
						score: 0,
					});
				}
				drawTeams();
				nextWord();
				document.getElementById("next").onclick=nextWord
			};
		}
	};
	let bar = document.getElementById("prog");
	req.onprogress = function (ev) {
		if (ev.lengthComputable){
			bar.max = ev.total;
			bar.value = ev.loaded;
		}
	};
	req.open("GET", url, true);
	req.send(null);
}

load();