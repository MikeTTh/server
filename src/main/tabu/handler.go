package tabu

import (
	"net/http"
)

var srcDir = "./src/main/tabu/"

func HandleTabu(w http.ResponseWriter, r *http.Request) {
	switch r.URL.Path {
	case "/tabu/", "/tabu/index", "/tabu/index.html":
		http.ServeFile(w, r, "src/main/tabu/index.html")
		break
	case "/tabu/index.js":
		http.ServeFile(w, r, "src/main/tabu/index.js")
		break
	default:
		break
	}
}
