package m_cache

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"
)

// TODO: Time for a rewrite

type Data struct {
	generated time.Time
	data      []byte
	header    http.Header
	status    *int
}

var storage = make(map[string]Data)

type mResponseWriter struct {
	http.ResponseWriter
	path string
}

func (w *mResponseWriter) WriteHeader(h int) {
	d := storage[w.path]
	d.status = &h
	storage[w.path] = d
	w.ResponseWriter.WriteHeader(h)
}

func (w *mResponseWriter) Write(b []byte) (int, error) {
	d := storage[w.path]
	d.data = append(d.data, b...)
	if len(d.header) == 0 {
		d.header = w.Header()
	} else {
		for key, val := range w.Header() {
			for _, v := range val {
				d.header.Set(key, v)
			}
		}
	}
	storage[w.path] = d
	i, err := w.ResponseWriter.Write(b)
	return i, err
}

func serveFromCache(w http.ResponseWriter, r *http.Request) bool {
	path := r.RequestURI
	if val, ok := storage[path]; ok {
		cc := strings.Split(val.header.Get("Cache-Control"), "max-age=")
		if len(cc) <= 1 {
			return false
		}
		dur, err := strconv.Atoi(cc[1])
		if err != nil {
			return false
		}
		if int(time.Now().Sub(val.generated).Seconds()) >= dur {
			return false
		}
		if *val.status == http.StatusNotFound && int(time.Now().Sub(val.generated).Seconds()) > 60 {
			return false
		}
		for k, val1 := range val.header {
			for _, v := range val1 {
				w.Header().Set(k, v)
			}
		}
		w.WriteHeader(*val.status)
		w.Write(val.data)
		return true
	}
	return false
}

func Cache(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		t := time.Now()
		c := serveFromCache(w, r)
		if c {
			fmt.Println(" (c)")
		} else {
			storage[r.RequestURI] = Data{
				generated: t,
				data:      []byte{},
				header:    http.Header{},
				status:    nil,
			}
			mw := &mResponseWriter{
				ResponseWriter: w,
				path:           r.RequestURI,
			}
			fmt.Println(" (g)")
			h.ServeHTTP(mw, r)
		}
	})
}

func NoCache(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Println(" (n)")
		h.ServeHTTP(w, r)
	})
}
