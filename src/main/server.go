package main

import (
	"bufio"
	"crypto/tls"
	"flag"
	"net/http"
	"os"
	"regexp"
	"strings"
	"time"

	"github.com/NYTimes/gziphandler"
	"github.com/tdewolff/minify"
	"github.com/tdewolff/minify/css"
	"github.com/tdewolff/minify/html"
	"github.com/tdewolff/minify/js"
	"github.com/tdewolff/minify/json"
	"github.com/tdewolff/minify/svg"
	"github.com/tdewolff/minify/xml"
	"golang.org/x/crypto/acme/autocert"

	"./clock"
	"./m_autocert"
	"./m_cache"
	"./m_ddns"
	"./m_middle"
	"./sourcemap"
	"./tabu"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func init() {}

func handleFiles(w http.ResponseWriter, r *http.Request) {
	filename := r.URL.Path[1:]
	w.Header().Set("Cache-Control", "public, max-age=86400")
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		w.Header().Set("Cache-Control", "public, max-age=86400")
		w.WriteHeader(http.StatusNotFound)
		http.ServeFile(w, r, "root/404.html")
	} else {
		http.ServeFile(w, r, filename)
	}
}

func handleRoot(w http.ResponseWriter, r *http.Request) {
	filename := "root/" + r.URL.Path[1:]
	w.Header().Set("Cache-Control", "public, max-age=86400")
	switch r.URL.Path {
	case "/", "/index", "/index.php":
		http.ServeFile(w, r, "root/index.html")
	default:
		if _, err := os.Stat(filename); os.IsNotExist(err) {
			w.Header().Set("Cache-Control", "public, max-age=86400")
			w.WriteHeader(http.StatusNotFound)
			http.ServeFile(w, r, "root/404.html")
		} else {
			http.ServeFile(w, r, filename)
		}
	}
}

func handlShrt(w http.ResponseWriter, r *http.Request) {
	id := strings.Split(r.URL.Path, "shrt/")[1]
	file, err := os.Open("shrt.txt")
	check(err)
	scan := bufio.NewScanner(file)
	for scan.Scan() {
		arr := strings.Split(scan.Text(), ": ")
		if id == arr[0] {
			http.Redirect(w, r, arr[1], http.StatusTemporaryRedirect)
			return
		}
	}
	w.Header().Set("Cache-Control", "public, max-age=86400")
	w.WriteHeader(http.StatusNotFound)
	http.ServeFile(w, r, "root/404.html")
}

func makeHTTPServer(mux http.Handler) *http.Server {
	return &http.Server{
		// ReadTimeout:  5 * time.Second,
		// WriteTimeout: 5 * time.Second,
		IdleTimeout: 120 * time.Second,
		Handler:     mux,
	}
}

func main() {
	dd := flag.Bool("prod", false, "enable production mode")
	flag.Parse()

	cached := http.NewServeMux()

	clock.Initialize()
	var ch http.Handler
	ch = http.HandlerFunc(clock.ClockHandler)

	var tb http.Handler
	tb = http.HandlerFunc(tabu.HandleTabu)

	var hfm http.Handler
	hfm = http.HandlerFunc(handleFiles)

	var hr http.Handler
	hr = http.HandlerFunc(handleRoot)

	hf := sourcemap.SourceMap(*dd)(http.HandlerFunc(handleFiles))
	cached.Handle("/out/", hf)
	//cached.HandleFunc("/files/", handleFiles)
	cached.HandleFunc("/shrt/", handlShrt)

	// Add minification
	if *dd {
		m := minify.New()
		m.AddFunc("text/css", css.Minify)
		m.AddFunc("text/html", html.Minify)
		m.AddFunc("image/svg+xml", svg.Minify)
		m.AddFuncRegexp(regexp.MustCompile("^(application|text)/(x-)?(java|ecma)script$"), js.Minify)
		m.AddFuncRegexp(regexp.MustCompile("[/+]json$"), json.Minify)
		m.AddFuncRegexp(regexp.MustCompile("[/+]xml$"), xml.Minify)
		ch = m.Middleware(ch)
		hfm = m.Middleware(hfm)
		hr = m.Middleware(hr)
	}

	// Add caching
	cached.Handle("/clock/", ch)
	cached.Handle("/tabu/", tb)
	cached.Handle("/res/", hfm)
	cached.Handle("/", hr)
	hndlr := m_middle.Middle(cached)

	hndlr = gziphandler.MustNewGzipLevelHandler(2)(hndlr)
	if *dd {
		hndlr = m_cache.NoCache(hndlr)
	} else {
		hndlr = m_middle.DebugLogger(hndlr)
	}
	mux := http.NewServeMux()
	mux.Handle("/files/", m_middle.NoCacheLogger(gziphandler.MustNewGzipLevelHandler(5)(http.Handler(http.HandlerFunc(handleFiles)))))
	mux.Handle("/", hndlr)
	hndlr = mux
	hndlr = m_middle.Logger(hndlr)

	var m *autocert.Manager

	if *dd {
		m_ddns.Ddns()

		s := makeHTTPServer(hndlr)
		autocert.HostWhitelist()
		hostPolicy := m_autocert.HostPolicy()
		m = &autocert.Manager{
			Prompt:     autocert.AcceptTOS,
			HostPolicy: hostPolicy,
			Cache:      autocert.DirCache("."),
		}
		s.Addr = ":443"
		s.TLSConfig = &tls.Config{GetCertificate: m.GetCertificate}
		go func() {
			err := s.ListenAndServeTLS("", "")
			check(err)
		}()

		s2 := makeHTTPServer(hndlr)
		s2.Addr = ":80"
		go func() {
			s2.ListenAndServe()
		}()

		h := m.HTTPHandler(nil)
		s3 := makeHTTPServer(h)
		s3.Addr = ":8080"
		s3.ListenAndServe()
	} else {
		http.ListenAndServe(":8080", hndlr)
	}
}
