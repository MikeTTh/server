package m_middle

import (
	"crypto/md5"
	"fmt"
	"net/http"
	"time"
)

type mResponseWriter struct {
	http.ResponseWriter
}

func (w *mResponseWriter) Write(b []byte) (int, error) {
	hash := md5.Sum(b)
	w.Header().Set("ETag", fmt.Sprintf("%x", hash))
	i, err := w.ResponseWriter.Write(b)
	return i, err
}

func Middle(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		mw := &mResponseWriter{
			ResponseWriter: w,
		}
		h.ServeHTTP(mw, r)
	})
}

func Logger(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		t := time.Now()
		fmt.Printf("%d/%d %d:%d => %s -> %s", int(t.Month()), t.Day(), t.Hour(), t.Minute(), r.RemoteAddr, r.URL.String())
		h.ServeHTTP(w, r)
	})
}

func DebugLogger(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Println(" (d)")
		h.ServeHTTP(w, r)
	})
}

func NoCacheLogger(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Println(" (n)")
		h.ServeHTTP(w, r)
	})
}
