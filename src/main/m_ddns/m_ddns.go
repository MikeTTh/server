package m_ddns

import (
	"bufio"
	"fmt"
	"net/http"
	"time"
)

var Ip string

type Domain struct {
	Host     string
	Domain   string
	Password string
}

func (d *Domain) Update(ip string) {
	_, err := http.Get(fmt.Sprintf("https://dynamicdns.park-your-domain.com/update?host=%s&domain=%s&password=%s&ip=%s", d.Host, d.Domain, d.Password, ip))
	if err != nil {
		fmt.Printf("Couldn't update %s to %s\n", d.Host+"."+d.Domain, ip)
	} else {
		fmt.Printf("Updated %s to %s\n", d.Host+"."+d.Domain, ip)
	}
}

func Ddns() {
	var domains []Domain
	domains = append(domains, Domain{"@", "mikesweb.site", "a5c22ca2c5934ff2b199892217c7e822"})
	domains = append(domains, Domain{"www", "mikesweb.site", "a5c22ca2c5934ff2b199892217c7e822"})
	domains = append(domains, Domain{"@", "mikeslittle.website", "5681933269554c08b6b5f7c59d44eb8b"})
	domains = append(domains, Domain{"www", "mikeslittle.website", "5681933269554c08b6b5f7c59d44eb8b"})
	go func() {
		for true {
			ipr, err := http.Get("http://checkip.amazonaws.com/")
			if err == nil {
				s := bufio.NewScanner(ipr.Body)
				prevIp := Ip
				Ip = ""
				for s.Scan() {
					Ip += s.Text()
				}
				if Ip != prevIp {
					for _, d := range domains {
						d.Update(Ip)
					}
				}
			} else {
				fmt.Println("Couldn't get ip")
			}
			time.Sleep(10 * time.Minute)
		}
	}()
}
