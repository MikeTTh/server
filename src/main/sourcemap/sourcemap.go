package sourcemap

import (
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

var sources map[string]string

func SourceMap(prod bool) func(handler http.Handler) http.Handler {
	if !prod {
		return func(h http.Handler) http.Handler {
			return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				filenames := strings.Split(r.URL.Path, "/")
				filename := filenames[len(filenames)-1]
				if filename[len(filename)-3:] == ".go" {
					if val, ok := sources[filename]; ok {
						if val != "" {
							http.ServeFile(w, r, val)
							fmt.Println(filename, " served from sources")
							return
						}
					} else {
						val := findSourceFile(filename)
						if val != "" {
							http.ServeFile(w, r, val)
							fmt.Println(filename, " served from sources")
							return
						}
					}
					w.WriteHeader(http.StatusNotFound)
					http.ServeFile(w, r, "root/404.html")
					return
				}
				h.ServeHTTP(w, r)
			})
		}
	}
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if r.URL.Path[:7] == ".js.map" {
				w.WriteHeader(http.StatusNotFound)
				http.ServeFile(w, r, "root/404.html")
				return
			}
			h.ServeHTTP(w, r)
		})
	}
}

func findSourceFile(file string) string {
	out := ""
	err := filepath.Walk("src", func(path string, info os.FileInfo, err error) error {
		if info.Name() == file {
			out = path
			return nil
		}
		return nil
	})
	if err != nil {
		panic(err)
	}
	return out
}
