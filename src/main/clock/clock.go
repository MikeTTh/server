package clock

import (
	"html/template"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

var cHtml *template.Template
var bDir = "./src/main/clock/"

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func Initialize() {
	var err error
	cHtml, err = template.ParseFiles(bDir + "clock.temp.html")
	check(err)
}

type Values struct {
	V url.Values
}

func (v *Values) Ioz(k string) int { // Int or zero
	n, err := strconv.Atoi(v.V.Get(k))
	if err == nil {
		return n
	} else {
		switch k {
		case "y":
			if v.V.Get("m") == "" {
				return time.Now().Year() + 1
			} else {
				return time.Now().Year()
			}
		case "m":
			return int(time.Now().Month())
		case "d":
			return 1
		case "h":
			return 1
		default:
			return 0
		}
	}
	return 0
}

type clockTemp struct {
	DateStr           string
	Date              string
	Y, M, D, H, Mi, S int
}

func ClockHandler(w http.ResponseWriter, r *http.Request) {
	v := &Values{
		r.URL.Query(),
	}
	date := time.Date(
		v.Ioz("y"),
		time.Month(v.Ioz("m")),
		v.Ioz("d"),
		v.Ioz("h"),
		v.Ioz("mi"),
		v.Ioz("s"),
		v.Ioz("ns"),
		time.Local,
	)

	y, m, d, h, mi, s := Diff(time.Now(), date)

	c := &clockTemp{
		DateStr: date.String(),
		Date:    date.Format(time.RFC3339),
		Y:       y, M: m, D: d, H: h, Mi: mi, S: s,
	}
	cHtml.Execute(w, c)
}
