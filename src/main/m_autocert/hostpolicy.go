package m_autocert

import (
	"fmt"

	"context"

	"golang.org/x/crypto/acme/autocert"
)

var validUrl = []string{"mikesweb.site", "mikeslittle.website"}

func HostPolicy() autocert.HostPolicy {
	return func(_ context.Context, host string) error {
		for _, u := range validUrl {
			cut := len(host) - len(u)
			if cut >= 0 {
				chost := host[cut:]
				if chost == u {
					return nil
				}
			}
		}
		return fmt.Errorf("acme/autocert: host %q not configured in HostWhitelist", host)
	}
}
