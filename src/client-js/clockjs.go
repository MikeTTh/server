package main

import (
	"fmt"
	"strconv"
	"sync"
	"time"

	"github.com/gopherjs/gopherwasm/js"
	"github.com/gowasm/go-js-dom"

	"../main/clock"
)

var date time.Time

func init() {
	s := js.Global().Get("date")
	var err error
	date, err = time.Parse(time.RFC3339, s.String())
	if err != nil {
		panic(err)
	}
}

func update(a []js.Value) {
	y, m, d, h, mi, s := clock.Diff(time.Now(), date)
	dom.GetWindow().Document().GetElementByID("Y").SetInnerHTML(strconv.Itoa(y))
	dom.GetWindow().Document().GetElementByID("M").SetInnerHTML(strconv.Itoa(m))
	dom.GetWindow().Document().GetElementByID("D").SetInnerHTML(strconv.Itoa(d))
	dom.GetWindow().Document().GetElementByID("H").SetInnerHTML(strconv.Itoa(h))
	dom.GetWindow().Document().GetElementByID("Mi").SetInnerHTML(strconv.Itoa(mi))
	dom.GetWindow().Document().GetElementByID("S").SetInnerHTML(strconv.Itoa(s))
}

func main() {
	fmt.Println("Now Interactive.")
	js.Global().Call("setInterval", js.NewCallback(update), "1000")
	var wg sync.WaitGroup
	wg.Add(1)
	wg.Wait()
}
